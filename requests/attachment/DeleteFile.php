<?php
namespace Empu\Support\Requests\Attachment;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Empu\Support\Repository\RepoFactory;
use Empu\Tenant\Facades\Team;

class DeleteFile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => ['required', Rule::refExists('attachmentFile')],
        ];
    }
}
