<?php
namespace Empu\Support\Rules;

use Illuminate\Contracts\Validation\Rule;
use Empu\Support\Repository\RepoFactory;

class RefExists implements Rule
{
    protected $modelName;

    public function __construct($modelName = null) {
        $this->modelName = $modelName;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $modelName = $this->modelName ?? $attribute;
        $model = RepoFactory::make($modelName)->getModel();

        return $model->byRef($value)->count() > 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute value not valid.';
    }
}
