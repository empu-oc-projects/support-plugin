<?php
namespace Empu\Support;

use Backend;
use Empu\Support\Classes\PhoneNumber;
use System\Classes\PluginBase;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Empu\Support\Rules\RefExists;
use Empu\Support\Repository\RepoFactory;
use Empu\Support\Repositories\AttachmentFileRepo;

/**
 * Support Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Support',
            'description' => 'No description provided yet...',
            'author'      => 'Empu',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        Rule::macro('refExists', function(string $modelName = null): RefExists {
            return new RefExists($modelName);
        });
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        RepoFactory::register([
            'attachmentFile' => AttachmentFileRepo::class,
        ]);

        Validator::extend('phone_number', function ($attribute, $value, $parameters, $validator) {
            try {
                PhoneNumber::validate($value);
            }
            catch (\Throwable $th) {
                return false;
            }

            return true;
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Empu\Support\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'empu.support.some_permission' => [
                'tab' => 'Support',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'support' => [
                'label'       => 'Support',
                'url'         => Backend::url('empu/support/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.support.*'],
                'order'       => 500,
            ],
        ];
    }
}
