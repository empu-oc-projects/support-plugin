<?php

namespace Empu\Support\Repository;

use Illuminate\Support\Str;
use October\Rain\Database\Builder;

/**
 * List query
 */
trait ListExtendQueryTrait
{
    protected function listFilterExtendQuery($query)
    {
        $request = app('request');
        // TODO: completely using query instead search
        $legacyTerm = $request->input('search');
        $term = $request->input('query', $legacyTerm);
        $fieldFilters = $request->input('filters', []);

        $this->textSearch($query, $term);
        $this->fieldFilters($query, $fieldFilters);
    }

    protected function textSearch(Builder $query, ?string $term): void
    {
        $cleanTerm = trim($term);
        $fields = (array)$this->searchableFields;

        if ($cleanTerm) {
            $query->where(function($query) use ($fields, $term) {
                foreach ($fields as $field) {
                    $query->orWhere($field, 'like', "%{$term}%");
                }

                if (method_exists($this, 'customTextSearch')) {
                    $this->customTextSearch($query, $term);
                }
            });
        }
    }

    protected function fieldFilters(Builder $query, array $fieldFilters): void
    {
        $fieldFilters = $this->sanitizeFieldFilters($fieldFilters);
        $fields = array_intersect(array_keys($fieldFilters), $this->filterableFields);
        
        if (count($fields) > 0) {
            $query->where(function (Builder $query) use ($fields, $fieldFilters) {
                foreach ($fields as $field) {
                    $applicatorMethod = 'apply'.Str::studly($field).'FieldFilter';
                    $value = $fieldFilters[$field];
                    
                    if (method_exists($this, $applicatorMethod)) {
                        $this->{$applicatorMethod}($query, $value);
                    }
                    else {
                        $query->whereIn($field, (array)$value);
                    }
                }
            });
        }
    }

    protected function sanitizeFieldFilters(array $fieldFilters): array
    {
        foreach ($fieldFilters as $key => &$value) {
            if (is_string($value)) {
                $value = $this->sanitizeString($value);
            }

            if (is_null($value) || (is_array($value) && ! count($value))) {
                unset($fieldFilters[$key]);
            }
        }

        return $fieldFilters;
    }

    protected function sanitizeString(string $value)
    {
        $value = trim($value);

        if ($value === '') {
            return null;
        }
        elseif ($value === 'false') {
            return false;
        }
        elseif ($value === 'true') {
            return true;
        }
        else {
            return $value;
        }
    }

    protected function listSortExtendQuery($query)
    {
        $field = request('sort');
        $direction = request('order', 'asc');

        if (empty($field) || ! in_array($field, $this->sortableFields)) {
            return;
        }

        $applicatorMethod = 'apply'.Str::studly($field).'FieldSort';
            
        if (method_exists($this, $applicatorMethod)) {
            $this->{$applicatorMethod}($query, $direction);
        }
        else {
            $query->orderBy($field, $direction);
        }
    }
}
