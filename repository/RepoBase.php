<?php
namespace Empu\Support\Repository;

use Illuminate\Database\Eloquent\Model;

abstract class RepoBase
{
    protected $model;

    public $identifierColumn = 'id';

    public function setModel(Model $model)
    {
        $this->model = $model;

        return $this;
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function queryFinder(array $identifiers, array $columns = ['*'])
    {
        return $this->query()
            ->select($columns)
            ->whereIn($this->identifierColumn, $identifiers);
    }

    public function find($identifier, array $columns = ['*'])
    {
        $record = $this->queryFinder((array)$identifier, $columns)
            ->firstOrFail();

        return $record;
    }
    
    public function findMany(array $identifiers, array $columns = ['*'])
    {
        $records = $this->queryFinder($identifiers, $columns)
            ->get();

        return $records;
    }

    public function getAll(array $columns = ['*'])
    {
        $listQuery = $this->listQuery();
        $records = $listQuery
            ->select($this->qualifyColumns($columns))
            ->get();

        return $records;
    }
    
    public function getPaginated(?int $pageSize = 15, array $columns = ['*'])
    {
        $pageSize = $pageSize ?? 15;
        $listQuery = $this->listQuery();
        $records = $listQuery
            ->select($this->qualifyColumns($columns))
            ->paginate($pageSize);

        return $records;
    }

    public function prepare(array $payload)
    {
        if (isset($payload['id']) && ! empty($payload['id'])) {
            $record = $this->find($payload['id']);
        }
        else {
            $record = $this->getModel();
        }

        $record->fill($payload);
        
        return $record;
    }

    public function create(array $payload, array $relations = [])
    {
        return $this->createOrUpdate($payload, $relations);
    }

    public function update(array $payload, array $relations = [])
    {
        return $this->createOrUpdate($payload, $relations);
    }

    public function createOrUpdate(array $payload, array $relations = [])
    {
        $record = $this->prepare($payload);

        $record->save();

        return $record;
    }

    public function delete($identifier)
    {
        $record = $this->find($identifier);
        
        return $record->delete();
    }

    public function deleteMany(array $identifiers)
    {
        return $this->query()
            ->whereIn($this->identifierColumn, $identifiers)
            ->delete();
    }

    public function query()
    {
        $query = $this->model->query();

        $this->extendQuery($query);

        return $query;
    }

    /**
     * Extending query
     *
     * @param [type] $query
     * @return void
     */
    protected function extendQuery($query)
    {
        // $query
    }
    
    public function listQuery()
    {
        $query = $this->query();

        $this->listExtendQuery($query);
        $this->listFilterExtendQuery($query);
        $this->listSortExtendQuery($query);

        return $query;
    }

    protected function listExtendQuery($query)
    {
        // $query
    }

    protected function listFilterExtendQuery($query)
    {
        // $query
    }

    protected function listSortExtendQuery($query)
    {
        // $query
    }

    protected function qualifyColumns(array $columns)
    {
        foreach ($columns as &$column) {
            $column = $this->getModel()->qualifyColumn($column);
        }

        return $columns;
    }
}
