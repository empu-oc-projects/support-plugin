<?php
namespace Empu\Support\Repository;

use App;
use Empu\Support\Contracts\RepositoryActor;
use Illuminate\Database\Eloquent\Relations\Relation;

class Factory
{
    public static function register(array $repoBinds): void
    {
        foreach ($repoBinds as $alias => $repository) {
            App::bind(self::bindingAlias($alias), $repository);
        }
    }

    public static function make($alias)
    {
        $alias = self::getAliasFromString($alias);

        return App::make(self::bindingAlias($alias));
    }

    public static function makeFromRelation(Relation $relation)
    {
        $model = $relation->getRelated();

        return self::makeFromModel($model);
    }

    public static function makeFromModel(RepositoryActor $model)
    {   
        $alias = $model->getRepositoryAlias();
        
        return App::make(self::bindingAlias($alias));
    }

    private static function bindingAlias(string $alias): string
    {
        return "repo.{$alias}";
    }

    private static function getAliasFromString(string $alias)
    {
        if (! class_exists($alias)) {
            return $alias;
        }

        $model = new $alias;
    
        if (! ($model instanceof RepositoryActor)) {
            throw new \ReflectionException(
                'Repositoy for class '.get_class($model).' does not exists'
            );
        }

        return $model->getRepositoryAlias();
    }
}
