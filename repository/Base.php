<?php
namespace Empu\Support\Repository;

use Empu\Support\Contracts\RepositoryActor;

abstract class Base
{
    protected $model;

    public $identifierColumn = 'id';

    public function setModel(RepositoryActor $model)
    {
        $this->model = $model;

        return $this;
    }

    public function getModel(): RepositoryActor
    {
        return $this->model;
    }

    public function find($identifier, array $columns = ['*'])
    {
        $record = $this->finderQuery($identifier, $columns)
            ->firstOrFail();

        return $record;
    }
    
    public function findMany(array $identifiers, array $columns = ['*'])
    {
        $records = $this->finderQuery($identifiers, $columns)
            ->get();

        return $records;
    }

    public function getAll(array $columns = ['*'])
    {
        return $this->listQuery($columns)
            ->get();
    }
    
    public function getPaginated(?int $pageSize = 15, array $columns = ['*'])
    {
        $pageSize = $pageSize ?? 15;

        return $this->listQuery($columns)
            ->paginate($pageSize);
    }

    public function prepare(array $payload = [], $persist = false)
    {
        if (isset($payload['id']) && ! empty($payload['id'])) {
            $record = $this->find($payload['id']);
        }
        else {
            $record = $this->getModel();
        }

        $this->prepareAttributes($record, $payload);

        if ($persist) {
            $record->save();
        }
        
        return $record;
    }

    protected function prepareAttributes($model, array $payload = [])
    {
        $model->fill($payload);
    }

    protected function prepareRelation($model, string $relationKey, $payload = null)
    {
        $preparationMethod = 'prepare' . studly_case($relationKey) . 'Relation';
        $payload = is_string($payload) ? ['id' => $payload] : $payload;

        if (method_exists($this, $preparationMethod)) {
            $this->{$preparationMethod}($model, $payload);
        }
        else {
            $relation = $this->getModel()->{$relationKey}();
            $repository = Factory::makeFromRelation($relation);
            
            $model->{$relationKey} = $repository->prepare($payload);
        }
    }

    public function create(array $payload)
    {
        return $this->createOrUpdate($payload);
    }

    public function update(array $payload)
    {
        return $this->createOrUpdate($payload);
    }

    public function createOrUpdate(array $payload)
    {
        $record = $this->prepare($payload);

        $this->persist($record);

        return $record;
    }

    public function persist($model)
    {
        $model->save();
    }

    public function delete($identifier)
    {
        $record = $this->find($identifier);
        
        return $record->delete();
    }

    public function deleteMany(array $identifiers)
    {
        return $this->finderQuery($identifiers)
            ->delete();
    }

    public function query()
    {
        $query = $this->model->query();

        $this->extendQuery($query);

        return $query;
    }

    /**
     * Extending query
     *
     * @param [type] $query
     * @return void
     */
    protected function extendQuery($query)
    {
        // $query
    }

    public function finderQuery($identifiers, array $columns = ['*'])
    {
        return $this->query()
            ->select($this->qualifyColumn($columns))
            ->whereIn($this->getIdentifierColumn(), (array)$identifiers);
    }
    
    public function listQuery(array $columns = ['*'])
    {
        $query = $this->query()
            ->select($this->qualifyColumn($columns));

        $this->listExtendQuery($query);
        $this->listFilterExtendQuery($query);
        $this->listSortExtendQuery($query);

        return $query;
    }

    protected function listExtendQuery($query)
    {
        // $query
    }

    protected function listFilterExtendQuery($query)
    {
        // $query
    }

    protected function listSortExtendQuery($query)
    {
        // $query
    }

    protected function getIdentifierColumn()
    {
        return $this->qualifyColumn($this->identifierColumn);
    }

    protected function qualifyColumn($column)
    {
        if (is_array($column)) {
            foreach ($column as &$col) {
                $col = $this->getModel()->qualifyColumn($col);
            }
        }
        else {
            $column = $this->getModel()->qualifyColumn($column);
        }

        return $column;
    }
}
