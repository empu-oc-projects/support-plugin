<?php /*** Bismillahirrahmanirrahim ***/

namespace Empu\Support\Libs;

/**
 * Refference number generator
 */
class RefNumber
{
    /**
     * Sequence digit length
     * @var integer
     */
    protected $seqDigitLength;

    protected $prefix;

    public function __construct($seqDigitLength = 3, $prefix = '')
    {
        $this->seqDigitLength = $seqDigitLength;
        $this->prefix = $prefix;
    }

    /**
     * Generate reference number in format <prefix><date><type><sequence><check>
     *
     * @param \Closure $sequenceChecker
     * @param integer $invType
     * @return string
     */
    public function generate(\Closure $sequenceChecker, int $invType = null)
    {
        // 211 0 999: date type sequence
        $invDate = self::condenseDate();
        $headNum = "{$this->prefix}{$invDate}{$invType}";
        $nextSeq = $this->generateSequence($headNum, $sequenceChecker);

        return "{$headNum}{$nextSeq}";
    }

    protected function generateSequence($headNum, $sequenceChecker)
    {
        $count = $sequenceChecker($headNum);
        $next = ++$count;
        $max = str_repeat(9, $this->seqDigitLength);

        if ($next > (int)$max) {
            throw new \Exception(
                'Sequence value has been reached max number. Cannot add more number.'
            );
        }

        return str_pad($next, $this->seqDigitLength, '0', STR_PAD_LEFT);
    }

    /**
     * Menghasilkan date yg ringkas tampilannya
     * @param  string $monthDate teks bulan dan tanggal
     * @return string            tanggal ringkas
     */
    public static function condenseDate(\DateTime $date = null, $withYear = true) {
        $date = $date ?: new \DateTime;
        $month = $date->format('m');
        $day = $date->format('d');

        $h = (floor($day/10) * 2) + floor($month/10);
        $t = $month % 10;
        $u = $day % 10;
        
        return ($withYear ? $date->format('y') : '') . "{$h}{$t}{$u}";
    }

    /**
     * Mengembalikan tanggal dalam format `md`
     * @param  string $condensed    tanggal ringkas
     * @return string               teks bulan dan tanggal
     */
    public static function expandDate($condensedDate) {
        if (! preg_match('~(\d{2})?(\d{3})$~', $condensedDate, $matches)) {
            return false;
        }
        
        list($h, $t, $u) = str_split($matches[2]);
        $day = floor($h/2).$u;
        $month = ($h%2).$t;

        if ($matches[1]) {
            $format = 'ymd';
            $dateStr = "{$matches[1]}{$month}{$day}";
        }
        else {
            $format = 'md';
            $dateStr = "{$month}{$day}";
        }

        return \DateTime::createFromFormat($format, $dateStr);
    }
}
