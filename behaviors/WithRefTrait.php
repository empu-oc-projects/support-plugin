<?php
namespace Empu\Support\Behaviors;

use Ramsey\Uuid\Uuid;

trait WithRefTrait
{
    public function generateRef($force = false)
    {
        $refKeyName = $this->getRefKeyName();

        if (method_exists($this, 'refGenerator')) {
            $ref = $this->refGenerator();
        }
        else {
            $ref = Uuid::uuid4();
        }

        if (empty($this->{$refKeyName}) || $force) {
            $this->{$refKeyName} = $ref;
        }
    }

    public function getRefAttribute()
    {
        $refKeyName = $this->getRefKeyName();

        return $this->{$refKeyName};
    }

    public function getRefKeyName()
    {
        return isset($this->refKey) ? $this->refKey : $this->primaryKey;
    }

    public function scopeByRef($query, $refs)
    {
        return $query->whereIn($this->getRefKeyName(), (array)$refs);
    }

    public function scopefindRef($query, $refs)
    {
        return $this->scopeByRef($query, $refs)->firstOrFail();
    }
}
