<?php
namespace Empu\Support\Behaviors;

use October\Rain\Extension\ExtensionBase;
use October\Rain\Support\Collection;
use Empu\Dash\Behaviors\AjaxResponseTrait;
use Empu\Support\Transformers\FileTransformer;
use Empu\Support\Models\AttachmentFile;
use Empu\Support\Requests\Attachment as FormRequests;
use Empu\Support\Repository\RepoFactory;

class ManageFileComponent extends ExtensionBase
{
    use AjaxResponseTrait;

    protected $component;

    public function __construct($component)
    {
        $this->component = $component;
        
        $this->setTransformer(new FileTransformer);
    }

    public function onUploadFile()
    {
        $field = request()->get('file_field_name', 'upload_file');
        $payload = request()->file($field);
        $isPublic = request()->get('is_public', true);
        
        if (is_array($payload)) {
            $data = $this->processFiles($payload, $isPublic);
        }
        else {
            $data = $this->saveFile($payload, $isPublic);
        }

        return $this->transform($data);
    }

    public function onDeleteFile()
    {
        try {
            $request = resolve(FormRequests\DeleteFile::class);
            $ref = $request->get('file');
            $result = RepoFactory::make('attachmentFile')->delete($ref);
        }
        catch (\Throwable $th) {
            $this->handleException($th);
        }

        return $result;
    }

    protected function processFiles($payload, $isPublic = true)
    {
        $files = new Collection;

        foreach ($payload as $data) {
            $file = $this->saveFile($data, $isPublic);
            $files->push($file);
        }

        return $files;
    }

    protected function saveFile($data, $isPublic)
    {
        $file = new AttachmentFile();

        $file->data = $data;
        $file->is_public = $isPublic;
        $file->save();

        return $file;
    }
}
