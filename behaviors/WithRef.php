<?php
namespace Empu\Support\Behaviors;

use October\Rain\Extension\ExtensionBase;
use Ramsey\Uuid\Uuid;

class WithRef extends ExtensionBase
{
    protected $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function generateRef()
    {
        $refKeyName = $this->getRefKeyName();
        $this->model->{$refKeyName} = Uuid::uuid4();
    }

    public function getRefAttribute()
    {
        $refKeyName = $this->getRefKeyName();

        return $this->model->{$refKeyName};
    }

    public function getRefKeyName()
    {
        return isset($this->model->refKey)
            ? $this->model->refKey : $this->model->primaryKey;
    }

    public function scopeByRef($query, $ref)
    {
        return $query->where($this->getRefKeyName(), $ref);
    }
}
