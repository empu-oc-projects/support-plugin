<?php

namespace Empu\Support\Classes;

use Empu\Support\Exceptions\InvalidPhoneNumberException;

class PhoneNumber
{
    public static function tidyUp($number, $prefixCode = 62)
    {
        $tidyNumber = preg_replace('~(?<!^)\D|^[^+\d]~', '', $number);
        $pattern = '~^([\+0]?'.$prefixCode.'|0)?(\d+)$~';
        $good = preg_match($pattern, $tidyNumber, $matches);

        // reformat dengan awalan kode
        return $good ? $prefixCode.$matches[2] : $tidyNumber;
    }

    public static function validate($number, $prefixCode = 62, $min = 7, $max = 12)
    {
        $tidyNumber = self::tidyUp($number, $prefixCode);
        $pattern = '~^('.$prefixCode.')?(\d{'.$min.','.$max.'})$~';
        $good = preg_match($pattern, $tidyNumber, $matches);
        $subsLength = strlen($matches[2]);

        if (! $good ||  $subsLength < $min || $subsLength > $max) {
            throw new InvalidPhoneNumberException();
        }

        return $tidyNumber;
    }
}
