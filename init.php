<?php

use Ramsey\Uuid\Uuid;

/**
 * Register namespaced uuid (uuid v5) helper
 */
if (! function_exists('UuidNs')) {
    /**
     * Namespaced uuid v5 helper
     *
     * @param string $namespace
     * @param mixed $value
     * @return \Ramsey\Uuid\Uuid
     */
    function UuidNs(string $namespace, $value) {
        $ns = Uuid::uuid5(Uuid::NIL, $namespace);

        return Uuid::uuid5($ns, $value);
    }
}
