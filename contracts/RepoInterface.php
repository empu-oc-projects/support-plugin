<?php
namespace Empu\Support\Contracts;

interface RepoInterface
{
    public function find($identifier);

    public function getAll(array $columns = ['*']);

    public function getPaginated(?int $pageSize, array $columns = ['*']);

    public function create(array $payload, array $relations = []);

    public function update(array $payload, array $relations = []);

    public function delete($identifier);
}
